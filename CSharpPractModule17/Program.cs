﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;

namespace CSharpPractModule17
{
    class Program
    {
        static void Main(string[] args)
        {
            string urlPath = "https://samples.openweathermap.org/data/2.5/find?q=London&type=like&mode=xml&appid=b6907d289e10d714a6e88b30761fae22";
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(urlPath);
            XmlElement rootElement = xmlDocument.DocumentElement;

            string cityName = rootElement.ChildNodes[3].ChildNodes[0].ChildNodes[0].Attributes["name"].Value;
            string country = rootElement.ChildNodes[3].ChildNodes[0].ChildNodes[0].ChildNodes[1].LastChild.Value;
            string sunRise = rootElement.ChildNodes[3].ChildNodes[0].ChildNodes[0].ChildNodes[2].Attributes["rise"].Value;
            string sunSet = rootElement.ChildNodes[3].ChildNodes[0].ChildNodes[0].ChildNodes[2].Attributes["set"].Value;
            string temperature = rootElement.ChildNodes[3].ChildNodes[0].ChildNodes[1].Attributes["value"].Value;
            string windSpeed = rootElement.ChildNodes[3].ChildNodes[0].ChildNodes[4].ChildNodes[0].Attributes["value"].Value;
            string windName = rootElement.ChildNodes[3].ChildNodes[0].ChildNodes[4].ChildNodes[0].Attributes["name"].Value;

            Console.WriteLine("City is " + cityName);
            Console.WriteLine("Country is " + country);
            Console.WriteLine("Sun Rise at " + sunRise);
            Console.WriteLine("Sun Set at " + sunSet);
            Console.WriteLine("Temperature is " + temperature + " Kelvins");
            Console.WriteLine("Wind Speed is " + windSpeed);
            Console.WriteLine("Wind Name is " + windName);


            Console.ReadLine();
        }
    }
}
